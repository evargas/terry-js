const ERROR_DE_NUMERO = "debes escribir un numero"
const ERROR_DE_OPERADOR = "debes escoger un operador entre suma, resta, multi o divide"

const operacion = (a, b, operador) => {
    const parse1 = parseFloat(a)
    const parse2 = parseFloat(b)

    if(isNaN(parse1) || isNaN(parse2)){
       return ERROR_DE_NUMERO
    }

    switch (operador) {
        case 'suma':
            return parse1 + parse2
        case 'resta':
            return parse1 - parse2
        case 'multi':
            return parse1 * parse2
        case 'divide':
            return parse1 / parse2
        default:
            return ERROR_DE_OPERADOR
    }
}

const handleClick = (opcion) => {
    let input1 = document.getElementById("input1").value
    let input2 = document.getElementById("input2").value
    let modal = document.getElementById("modal-container");
    modal.classList.add("active");
    const resultado = operacion(input1,input2, opcion)
    let element = document.getElementById("modal");
    element.innerHTML = resultado
    document.getElementById("input1").value = !isNaN(resultado) ? resultado : 0
}

const handleClose = () => document.getElementById("modal-container").classList.remove("active")

const handleClear = () => {
    document.getElementById("input1").value = 0
    document.getElementById("input2").value = null
}